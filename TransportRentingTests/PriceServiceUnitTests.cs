using System;
using Xunit;
using TransportRenting.Core;
using MvvmCross.Tests;
using Moq;
using TransportRenting.Core.Services;
using TransportRenting.Core.Models;

namespace TransportRentingTests
{
    public class PriceServiceUnitTests : MvxIoCSupportingTest
    {
        // Prices without discounts.
        [Theory]
        [InlineData("12.12.2019 09:00", "13.12.2019 09:00", 10.0, 240.0)]
        [InlineData("12.12.2019 09:00", "12.12.2019 10:00", 10.0, 10.0)]
        [InlineData("12.12.2019 09:00", "20.12.2019 09:00", 10.0, 1920.0)]
        public void CalculatePrice_CorrectParameters_ReturnsCorrectAnswer(string fromDate, string toDate, double basePrice, double expected)
        {
            base.Setup();

            var timeServiceMock = new Mock<ITimeService>();
            var discounService = new Mock<IDiscountService>();
            DateTime.TryParse(fromDate, out DateTime time1);
            DateTime.TryParse(toDate, out DateTime time2);

            timeServiceMock
                .Setup(x => x.CalculateNumberOfHours(time1, time2))
                .Returns((time2 - time1).TotalHours);

            discounService
                .Setup(x => x.CalculateDiscountedPrice(It.IsAny<double>(), It.IsAny<int>(), It.IsAny<string>()))
                .Returns(basePrice);

            IPriceService priceService = new PriceService(timeServiceMock.Object, discounService.Object);
            Assert.Equal(expected, priceService.CalculatePrice(basePrice, time1, time2));
        }
    }
}
