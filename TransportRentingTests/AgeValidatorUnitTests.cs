﻿using System;
using Xunit;
using TransportRenting.Core;
using MvvmCross.Tests;
using Moq;
using TransportRenting.Core.Services;
using TransportRenting.Core.Models;
using TransportRenting.Core.Utilities;

namespace TransportRentingTests
{
    public class AgeValidatorUnitTests : MvxIoCSupportingTest
    {
        [Theory]
        [InlineData("1.1.2001", "1.1.2019", 18)]
        [InlineData("1.1.2000", "1.1.2019", 19)]
        [InlineData("1.1.2000", "12.1.2019", 18)]
        [InlineData("1.1.2000", "12.11.2019", 18)]
        [InlineData("11.5.2000", "12.11.2019", 1)]
        public void ValidateAge_ValidAge_ReturnsTrue(string birthDate, string timeNow, int ageLimit)
        {
            DateTime.TryParse(birthDate, out DateTime birth);
            DateTime.TryParse(timeNow, out DateTime now);

            var user = new User() { Birthdate = birth };

            var ageValidator = new AgeValidation();
            Assert.True(ageValidator.ValidateAge(user, now, ageLimit));
        }

        [Theory]
        [InlineData("1.1.2002", "1.1.2019", 18)]
        [InlineData("1.1.2009", "1.1.2019", 18)]
        [InlineData("1.2.2001", "1.1.2019", 18)]
        [InlineData("3.1.2001", "2.1.2019", 18)]
        [InlineData("1.1.2009", "1.1.2019", 100)]
        public void ValidateAge_InvalidAge_ReturnsFalse(string birthDate, string timeNow, int ageLimit)
        {
            DateTime.TryParse(birthDate, out DateTime birth);
            DateTime.TryParse(timeNow, out DateTime now);

            var user = new User() { Birthdate = birth};

            var ageValidator = new AgeValidation();
            Assert.False(ageValidator.ValidateAge(user, now, ageLimit));
        }
    }
}
