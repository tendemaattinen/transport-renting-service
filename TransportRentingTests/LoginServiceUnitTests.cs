﻿using System;
using Xunit;
using TransportRenting.Core;
using MvvmCross.Tests;
using Moq;
using TransportRenting.Core.Services;
using TransportRenting.Core.Models;

namespace TransportRentingTests
{
    public class LoginServiceUnitTests : MvxIoCSupportingTest
    {
        [Fact]
        public void Login_CorrectCreditals_ReturnsTrue()
        {
            var databaseServiceMock = new Mock<IDatabaseService>();
            var userServiceMock = new Mock<IUserService>();
            var userMock = new Mock<User>();
            userMock.Object.Username = "Testname";

            string fakeMessage;

            databaseServiceMock
                .Setup(x => x.GetUserFromDatabase(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Action<string>>()))
                .Returns(userMock.Object);

            ILoginService loginService = new LoginService(databaseServiceMock.Object, userServiceMock.Object);
            Assert.True(loginService.Login(It.IsAny<string>(), It.IsAny<string>(), (message) => fakeMessage = message));
        }

        [Fact]
        public void Login_WrongCreditals_ReturnsFalse()
        {
            var databaseServiceMock = new Mock<IDatabaseService>();
            var userServiceMock = new Mock<IUserService>();
            string fakeMessage;
            User user = null;

            databaseServiceMock
                .Setup(x => x.GetUserFromDatabase(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Action<string>>()))
                .Returns(user);

            ILoginService loginService = new LoginService(databaseServiceMock.Object, userServiceMock.Object);
            Assert.False(loginService.Login(It.IsAny<string>(), It.IsAny<string>(), (message) => fakeMessage = message));
        }
    }
}
