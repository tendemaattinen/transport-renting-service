﻿using System;
using Xunit;
using TransportRenting.Core;
using MvvmCross.Tests;
using Moq;
using TransportRenting.Core.Services;
using TransportRenting.Core.Models;

namespace TransportRentingTests
{
    public class PasswordServiceUnitTests : MvxIoCSupportingTest
    {
        [Theory]
        [InlineData("12345", "12345")]
        [InlineData("testPassword", "testPassword")]
        public void CheckIfPasswordsMatch_MatchingPasswords_ReturnsTrue(string hash, string password)
        {
            IPasswordService passwordService = new PasswordService();
            Assert.True(passwordService.CheckIfPasswordsMatch(passwordService.GenerateHash(hash), password));
        }

        [Theory]
        [InlineData("12342432", "12345")]
        [InlineData("testPassword", "testPassworD")]
        [InlineData(" testPassword", "testPassword")]
        [InlineData("testPassword", "")]
        public void CheckIfPasswordsMatch_NotMatchingPasswords_ReturnsFalse(string hash, string password)
        {
            IPasswordService passwordService = new PasswordService();
            Assert.False(passwordService.CheckIfPasswordsMatch(passwordService.GenerateHash(hash), password));
        }
    }
}
