﻿using System;
using Xunit;
using TransportRenting.Core;
using MvvmCross.Tests;
using Moq;
using TransportRenting.Core.Services;
using TransportRenting.Core.Models;
using Microsoft.EntityFrameworkCore.InMemory;
using Microsoft.EntityFrameworkCore;
using TransportRenting.Core.Database;

namespace TransportRentingTests
{
    // TODO
    // Whole class.
    public class DatabaseServiceUnitTests : MvxIoCSupportingTest
    {
        private IDatabaseService CreateDatabaseService()
        {
            var passwordServiceMock = new Mock<IPasswordService>();
            var orderServiceMock = new Mock<IOrderService>();
            var userServiceMock = new Mock<IUserService>();




            return new DatabaseService(passwordServiceMock.Object, orderServiceMock.Object, userServiceMock.Object);
        }

        /*
        [Fact]
        public void AddNewAccountToDatabase_CorrectParameters_ReturnsTrue()
        {

            var options = new DbContextOptionsBuilder<DatabaseContext>()
                .UseInMemoryDatabase(databaseName: "Users")
                .Options;

            DateTime.TryParse("1.1.1997", out DateTime birthdate);

            using (var context = new DatabaseContext(options))
            {
                context.Users.Add(new UserDb { UserId = 1, Username = "testname", Password = "12345",
                    Email = "test@te.st", IsAdmin = false, FirstName = "Tester",
                    LastName = "Test", Birthdate = birthdate});
                context.SaveChanges();
            }

            var databaseService = CreateDatabaseService();

        }
        */
    }
}
