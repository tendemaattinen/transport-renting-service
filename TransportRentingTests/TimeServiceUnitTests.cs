﻿using System;
using Xunit;
using MvvmCross.Tests;
using TransportRenting.Core.Services;

namespace TransportRentingTests
{
    public class TimeServiceUnitTests : MvxIoCSupportingTest
    {
        [Theory]
        [InlineData("1.1.2019 09:00", "2.1.2019 09:00", 24)]
        [InlineData("1.1.2019 09:00", "1.2.2020 09:00", 9504)]
        public void CalculateNumberOfHours_CorrectParameters_ReturnsCorrectAnswer(string start, string end, double expected)
        {
            DateTime.TryParse(start, out DateTime startDate);
            DateTime.TryParse(end, out DateTime endDate);
            ITimeService timeService = new TimeService();
            Assert.Equal(expected, timeService.CalculateNumberOfHours(startDate, endDate));
        }
    }
}
