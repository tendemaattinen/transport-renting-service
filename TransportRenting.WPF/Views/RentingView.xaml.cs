﻿using MvvmCross.Platforms.Wpf.Views;

namespace TransportRenting.WPF.Views
{
    public partial class RentingView : MvxWpfView
    {
        public RentingView()
        {
            InitializeComponent();
        }

        private void DataGrid_AutoGeneratingColumn(object sender, System.Windows.Controls.DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName == "VehicleId" || e.PropertyName == "LocationId")
            {
                e.Cancel = true;
            }
            else if (e.PropertyName == "BasePrice")
            {
                e.Column.Header = "€/h";
            }
        }
    }
}
