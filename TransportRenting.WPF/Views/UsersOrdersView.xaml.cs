﻿using MvvmCross.Platforms.Wpf.Views;
using System.Windows.Controls;

namespace TransportRenting.WPF.Views
{
    public partial class UsersOrdersView : MvxWpfView
    {
        public UsersOrdersView()
        {
            InitializeComponent();
        }

        private void DataGrid_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName == "UserId")
            {
                e.Cancel = true;
            }
            
        }
    }
}
