﻿using MvvmCross.Platforms.Wpf.Views;

namespace TransportRenting.WPF.Views
{
    /// <summary>
    /// Interaction logic for SettingsView.xaml
    /// </summary>
    public partial class SettingsView : MvxWpfView
    {
        public SettingsView()
        {
            InitializeComponent();
        }
    }
}
