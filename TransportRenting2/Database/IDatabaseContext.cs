﻿using Microsoft.EntityFrameworkCore;

namespace TransportRenting.Core.Database
{
    public interface IDatabaseContext
    {
        DbSet<LocationDb> Locations { get; set; }
        DbSet<OrderDb> Orders { get; set; }
        DbSet<UserDb> Users { get; set; }
        DbSet<VehicleDb> Vehicles { get; set; }
    }
}