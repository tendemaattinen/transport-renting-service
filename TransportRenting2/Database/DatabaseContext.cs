﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace TransportRenting.Core.Database
{
    public class PostgreSqlContext : DbContext, IDatabaseContext
    {
        public DbSet<UserDb> Users { get; set; }
        public DbSet<VehicleDb> Vehicles { get; set; }
        public DbSet<LocationDb> Locations { get; set; }
        public DbSet<OrderDb> Orders { get; set; }
        public DbSet<DiscountDb> Discounts { get; set; }
        public DbSet<DiscountCodesDb> DiscountCodes { get; set; }

        public PostgreSqlContext()
        {

        }
        public PostgreSqlContext(DbContextOptions<PostgreSqlContext> options) : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder.UseNpgsql(Properties.Resources.ConnectionString);

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.ForNpgsqlUseIdentityColumns();
            modelBuilder.HasDefaultSchema("public");
        }
    }

    /*public class SqlServerContext : DbContext, IDatabaseContext
    {
        public DbSet<UserDb> Users { get; set; }
        public DbSet<VehicleDb> Vehicles { get; set; }
        public DbSet<LocationDb> Locations { get; set; }
        public DbSet<OrderDb> Orders { get; set; }

        public SqlServerContext()
        {

        }
        public SqlServerContext(DbContextOptions<SqlServerContext> options) : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder.UseSqlServer(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\teemu\OneDrive\Repos\TransportRenting2\TransportRenting2\bin\Debug\netstandard2.0\Database\TransportRentingDB.mdf;Integrated Security=True;Connect Timeout=30");

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.ForSqlServerUseIdentityColumns();
        }
    }*/

    public class UserDb
    {
        [Key]
        public Guid UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsAdmin { get; set; }
        public DateTime Birthdate { get; set; }
    }

    public class VehicleDb
    {
        [Key]
        public Guid VehicleId { get; set; }
        public string Maker { get; set; }
        public string Model { get; set; }
        public int Year { get; set; }
        public string Type { get; set; }
        public double BasePrice { get; set; }
        public Guid LocationId { get; set; }
        public LocationDb Location { get; set; }
    }

    public class LocationDb
    {
        [Key]
        public Guid LocationId { get; set; }
        public string LocationName { get; set; }
    }

    public class OrderDb
    {
        [Key]
        public Guid OrderId { get; set; }
        public Guid VehicleId { get; set; }
        public VehicleDb Vehicle { get; set; }
        public Guid UserId { get; set; }
        public UserDb User { get; set; }
        public Guid LocationId { get; set; }
        public LocationDb Location { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public double Price { get; set; }
    }

    public class DiscountDb
    {
        [Key]
        public int DiscountId { get; set; }
        public int StartHour { get; set; }
        public double Factor { get; set; }
    }

    public class DiscountCodesDb
    {
        [Key]
        public int DiscountCodeId { get; set; }
        public string Code { get; set; }
        public double Factor { get; set; }
    }
}
