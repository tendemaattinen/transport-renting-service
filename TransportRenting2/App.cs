﻿using MvvmCross;
using MvvmCross.ViewModels;
using TransportRenting.Core.ViewModels;
using TransportRenting.Core.Services;
using TransportRenting.Core.Models;
using TransportRenting.Core.Utilities;
using TransportRenting.Core.Database;


namespace TransportRenting.Core
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            Mvx.IoCProvider.RegisterType<IPasswordService, PasswordService>();
            Mvx.IoCProvider.RegisterType<ILoginService, LoginService>();
            Mvx.IoCProvider.RegisterType<IDatabaseService, DatabaseService>();
            Mvx.IoCProvider.RegisterSingleton<IUserService>(() => new UserService());
            Mvx.IoCProvider.RegisterSingleton<IOrderService>(() => new OrderService());
            Mvx.IoCProvider.RegisterType<IUser, User>();
            Mvx.IoCProvider.RegisterType<IVehicle, Vehicle>();
            Mvx.IoCProvider.RegisterType<ILocation, Location>();
            Mvx.IoCProvider.RegisterType<IValidationService, ValidationService>();
            Mvx.IoCProvider.RegisterType<IOrder, Order>();
            Mvx.IoCProvider.RegisterType<IPriceService, PriceService>();
            Mvx.IoCProvider.RegisterType<ITimeService, TimeService>();
            Mvx.IoCProvider.RegisterType<IAgeValidation, AgeValidation>();
            Mvx.IoCProvider.RegisterType<IDiscountService, DiscountService>();
            Mvx.IoCProvider.RegisterType<IDatabaseContext, PostgreSqlContext>();

            RegisterAppStart<LoginViewModel>();
        }
    }
}
