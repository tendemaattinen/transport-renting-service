﻿using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using System;
using System.Threading.Tasks;
using TransportRenting.Core.Services;


namespace TransportRenting.Core.ViewModels
{
    class AddLocationViewModel : MvxViewModel
    {
        readonly IMvxNavigationService _navigationService;
        readonly IDatabaseService _databaseService;

        public AddLocationViewModel(IMvxNavigationService navigationService, IDatabaseService databaseService)
        {
            _navigationService = navigationService;
            _databaseService = databaseService;
        }
        public override async Task Initialize()
        {
            await base.Initialize();
        }

        private string _nameOfLocation;
        public string NameOfLocation
        {
            get => _nameOfLocation;
            set
            {
                if (value != null)
                {
                    _nameOfLocation = value;
                    RaisePropertyChanged(() => NameOfLocation);
                }
            }
        }

        private IMvxCommand _addNewLocation;
        public IMvxCommand AddNewLocation
        {
            get
            {
                if (_addNewLocation == null)
                {
                    _addNewLocation = new MvxCommand(() => AddLocationToDatabase());
                }
                return _addNewLocation;
            }
        }

        private void AddLocationToDatabase()
        {
            // TODO
            // Better checking.
            if (NameOfLocation != null)
            {
                MessageVisibility = "Visible";
                _databaseService.AddLocationToDataBase(NameOfLocation, (message) => Message = message);
                
            }
        }

        private IMvxCommand _goBack;
        public IMvxCommand GoBack
        {
            get
            {
                if (_goBack == null)
                {
                    _goBack = new MvxCommand(() =>
                    {
                        _navigationService.Navigate<SettingsViewModel>();
                    });
                }
                return _goBack;
            }
        }

        private string _messageVisibility = "Collapsed";
        public string MessageVisibility
        {
            get => _messageVisibility;
            set
            {
                if (value != null)
                {
                    _messageVisibility = value;
                    RaisePropertyChanged(() => MessageVisibility);
                }
            }
        }

        private string _message;
        public string Message
        {
            get => _message;
            set
            {
                if (value != null)
                {
                    _message = value;
                    RaisePropertyChanged(() => Message);
                    MessageVisibility = "Visible";
                }
            }
        }
    }
}
