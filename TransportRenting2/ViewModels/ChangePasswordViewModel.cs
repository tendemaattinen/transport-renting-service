﻿using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using System;
using System.Threading.Tasks;
using TransportRenting.Core.Models;
using TransportRenting.Core.Services;

namespace TransportRenting.Core.ViewModels
{
    class ChangePasswordViewModel : MvxViewModel
    {
        readonly IMvxNavigationService _navigationService;
        readonly IDatabaseService _databaseService;
        readonly IValidationService _validationService;
        public ChangePasswordViewModel(IMvxNavigationService navigationService, IDatabaseService databaseService, IValidationService validationService)
        {
            _navigationService = navigationService;
            _databaseService = databaseService;
            _validationService = validationService;
        }

        public override async Task Initialize()
        {
            await base.Initialize();
        }

        private IMvxCommand _goBack;
        public IMvxCommand GoBack
        {
            get
            {
                if (_goBack == null)
                {
                    _goBack = new MvxCommand(() =>
                    {
                        _navigationService.Navigate<SettingsViewModel>();
                    });
                }
                return _goBack;
            }
        }

        private string _messageVisibility = "Collapsed";
        public string MessageVisibility
        {
            get => _messageVisibility;
            set
            {
                if (value != null)
                {
                    _messageVisibility = value;
                    RaisePropertyChanged(() => MessageVisibility);
                }
            }
        }

        private string _message;
        public string Message
        {
            get => _message;
            set
            {
                if (value != null)
                {
                    _message = value;
                    RaisePropertyChanged(() => Message);
                    MessageVisibility = "Visible";
                }
            }
        }

        private string _oldPassword;
        public string OldPassword
        {
            get => _oldPassword;
            set
            {
                if (value != null)
                {
                    _oldPassword = value;
                    RaisePropertyChanged(() => OldPassword);
                }
            }
        }

        private string _newPassword;
        public string NewPassword
        {
            get => _newPassword;
            set
            {
                if (value != null)
                {
                    _newPassword = value;
                    RaisePropertyChanged(() => NewPassword);
                }
            }
        }

        private string _newPasswordAgain;
        public string NewPasswordAgain
        {
            get => _newPasswordAgain;
            set
            {
                if (value != null)
                {
                    _newPasswordAgain = value;
                    RaisePropertyChanged(() => NewPasswordAgain);
                }
            }
        }

        private IMvxCommand _changePassword;
        public IMvxCommand ChangePassword
        {
            get
            {
                if (_changePassword == null)
                {
                    _changePassword = new MvxCommand(() => ChangeUSersPassword());
                }
                return _changePassword;
            }
        }

        private void ChangeUSersPassword()
        {
            MessageVisibility = "Visible";
            if (!AreTextBoxesFilled())
            {
                Message = "Fill all textboxes!";
            }
            else if (!_databaseService.CheckIfPasswordMatches(OldPassword, (message) => Message = message))
            {
                
            }
            else if (!_validationService.CheckPasswordValidity(NewPassword, NewPasswordAgain, 5, 50, (message) => Message = message))
            {
                
            }
            else
            {
                _databaseService.ChangePassword(NewPassword, (message) => Message = message);
                EmptyTextBoxes();
            }
        }

        private bool AreTextBoxesFilled()
        {
            if ((string.IsNullOrWhiteSpace(OldPassword)) || (string.IsNullOrWhiteSpace(NewPassword)) 
                || (string.IsNullOrWhiteSpace(NewPasswordAgain)))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void EmptyTextBoxes()
        {
            OldPassword = string.Empty;
            NewPassword = string.Empty;
            NewPasswordAgain = string.Empty;
        }
    }
}
