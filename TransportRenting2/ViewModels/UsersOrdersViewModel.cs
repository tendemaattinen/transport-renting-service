﻿using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TransportRenting.Core.Models;
using TransportRenting.Core.Services;

namespace TransportRenting.Core.ViewModels
{
    class UsersOrdersViewModel : MvxViewModel
    {
        readonly IMvxNavigationService _navigationService;
        readonly IDatabaseService _databaseService;
        public UsersOrdersViewModel(IMvxNavigationService navigationService, IDatabaseService databaseService)
        {
            _navigationService = navigationService;
            _databaseService = databaseService;
        }

        public override async Task Initialize()
        {
            await base.Initialize();

            Orders = await Task.Run(() => _databaseService.GetOrders());
            await Task.Run(() => SetVehicle());
        }

        // TODO
        // Add vehicle and location information orders datagrid.
        // How is the question.
        private void SetVehicle()
        {
            Parallel.ForEach(Orders, order => order.Vehicle = _databaseService.GetVehicle(order.VehicleId, (message) => Message = message));
            RaisePropertyChanged(() => Orders);
        }

        private IEnumerable<IOrder> _orders;
        public IEnumerable<IOrder> Orders
        {
            get => _orders;
            set
            {
                if (value != null)
                {
                    _orders = value;
                    RaisePropertyChanged(() => Orders);
                }
            }
        }

        private IOrder _selectedOrder;
        public IOrder SelectedOrder
        {
            get => _selectedOrder;
            set
            {
                if (value != null)
                {
                    _selectedOrder = value;
                    RaisePropertyChanged(() => SelectedOrder);
                }
            }
        }
        private string _message;
        public string Message
        {
            get => _message;
            set
            {
                if (value != null)
                {
                    _message = value;
                    RaisePropertyChanged(() => Message);
                }
            }
        }


        private IMvxCommand _goBack;
        public IMvxCommand GoBack
        {
            get
            {
                if (_goBack == null)
                {
                    _goBack = new MvxCommand(() =>
                    {
                        _navigationService.Navigate<MainMenuViewModel>();
                    });
                }
                return _goBack;
            }
        }

        private IMvxCommand _cancelOrder;
        public IMvxCommand CancelOrder
        {
            get
            {
                if (_cancelOrder == null)
                {
                    _cancelOrder = new MvxAsyncCommand(() => Task.Run(() => CancelSelectedOrder()));
                }
                return _cancelOrder;
            }
        }

        private async Task CancelSelectedOrder()
        {
            if (SelectedOrder != null)
            {
                _databaseService.CancelOrder(SelectedOrder.OrderId, (message) => Message = message);
                Orders = await Task.Run(() => _databaseService.GetOrders());
            }
            else
            {
                Message = "Select order you want to cancel!";
            }
            
        }
    }
}
