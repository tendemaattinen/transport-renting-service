﻿using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using System;
using System.Threading.Tasks;
using TransportRenting.Core.Models;
using TransportRenting.Core.Services;

namespace TransportRenting.Core.ViewModels
{
    class SettingsViewModel : MvxViewModel
    {
        readonly IMvxNavigationService _navigationService;
        readonly IUserService _userService;
        public SettingsViewModel(IMvxNavigationService navigationService, IUserService userService)
        {
            _navigationService = navigationService;
            _userService = userService;
        }

        public override async Task Initialize()
        {
            await base.Initialize();

            if (_userService.CurrentUser.IsAdmin)
            {
                AddLocationVisibility = "Visible";
                AddVehicleVisibility = "Visible";
            }

        }

        private IMvxCommand _goBack;
        public IMvxCommand GoBack
        {
            get
            {
                if (_goBack == null)
                {
                    _goBack = new MvxCommand(() =>
                    {
                        _navigationService.Navigate<MainMenuViewModel>();
                    });
                }
                return _goBack;
            }
        }

        private IMvxCommand _changePersonalInformation;
        public IMvxCommand ChangePersonalInformation
        {
            get
            {
                if (_changePersonalInformation == null)
                {
                    _changePersonalInformation = new MvxCommand(() =>
                    {
                        _navigationService.Navigate<ChangeInformationViewModel>();
                    });
                }
                return _changePersonalInformation;
            }
        }

        private IMvxCommand _changePassword;
        public IMvxCommand ChangePassword
        {
            get
            {
                if (_changePassword == null)
                {
                    _changePassword = new MvxCommand(() =>
                    {
                        _navigationService.Navigate<ChangePasswordViewModel>();
                    });
                }
                return _changePassword;
            }
        }

        private IMvxCommand _createFakeDatabase;
        public IMvxCommand CreateFakeDatabase
        {
            get
            {
                if (_createFakeDatabase == null)
                {
                    //_createFakeDatabase = new MvxAsyncCommand(() => CreateDatabaseAsync());
                    _createFakeDatabase = new MvxCommand(() => CreateDatabase());
                }
                return _createFakeDatabase;
            }
        }

        // TODO
        // Not working as intended.
        // Disabled right now.
        /*public async Task CreateDatabaseAsync()
        {
            //await Task.Run(() => _databaseService.CreatePlaceholderDatabase());
            Console.WriteLine("Feature disabled.");
        }*/

        public void CreateDatabase()
        {
            Console.WriteLine("Feature disabled.");
        }

        private string _addLocationVisibility = "Collapsed";
        public string AddLocationVisibility
        {
            get => _addLocationVisibility;
            set
            {
                if (value != null)
                {
                    _addLocationVisibility = value;
                    RaisePropertyChanged(() => AddLocationVisibility);
                }
            }
        }

        private string _addVehicleVisibility = "Collapsed";
        public string AddVehicleVisibility
        {
            get => _addVehicleVisibility;
            set
            {
                if (value != null)
                {
                    _addVehicleVisibility = value;
                    RaisePropertyChanged(() => AddVehicleVisibility);
                }
            }
        }

        private IMvxCommand _addLocation;
        public IMvxCommand AddLocation
        {
            get
            {
                if (_addLocation == null)
                {
                    _addLocation = new MvxCommand(() => 
                    {
                        _navigationService.Navigate<AddLocationViewModel>();
                    });
                }
                return _addLocation;
            }
        }


        private IMvxCommand _addVehicle;
        public IMvxCommand AddVehicle
        {
            get
            {
                if (_addVehicle == null)
                {
                    _addVehicle = new MvxCommand(() => 
                    {
                        _navigationService.Navigate<AddVehicleViewModel>();
                    });
                }
                return _addVehicle;
            }
        }
    }
}
