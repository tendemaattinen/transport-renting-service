﻿using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using TransportRenting.Core.Models;
using TransportRenting.Core.Services;

namespace TransportRenting.Core.ViewModels
{
    class DateAndLocationViewModel : MvxViewModel
    {
        readonly IMvxNavigationService _navigationService;
        readonly IUserService _userService;
        readonly IDatabaseService _databaseService;
        readonly IOrderService _orderService;
        public DateAndLocationViewModel(IMvxNavigationService navigationService, IUserService userService, IDatabaseService databaseService, IOrderService orderService)
        {
            _navigationService = navigationService;
            _userService = userService;
            _databaseService = databaseService;
            _orderService = orderService;
        }

        public override async Task Initialize()
        {
            await base.Initialize();

            FromDate = DateTime.UtcNow.AddDays(1).ToString("dd.MM.yyyy");
            FromTime = "09:00";
            ToDate = DateTime.UtcNow.AddDays(8).ToString("dd.MM.yyyy");
            ToTime = "09:00";
            Locations = await Task.Run(() => _databaseService.GetLocations());
        }

        private string _message;
        public string Message
        {
            get => _message;
            set
            {
                if (value != null)
                {
                    _message = value;
                    RaisePropertyChanged(() => Message);
                }
            }
        }

        private string _fromDate;
        public string FromDate
        {
            get => _fromDate;
            set
            {
                if (value != null)
                {
                    _fromDate = value;
                    RaisePropertyChanged(() => FromDate);
                }
            }
        }

        private string _fromTime;
        public string FromTime
        {
            get => _fromTime;
            set
            {
                if (value != null)
                {
                    _fromTime = value;
                    RaisePropertyChanged(() => FromTime);
                }
            }
        }

        private string _toDate;
        public string ToDate
        {
            get => _toDate;
            set
            {
                if (value != null)
                {
                    _toDate = value;
                    RaisePropertyChanged(() => ToDate);
                }
            }
        }

        private string _toTime;
        public string ToTime
        {
            get => _toTime;
            set
            {
                if (value != null)
                {
                    _toTime = value;
                    RaisePropertyChanged(() => ToTime);
                }
            }
        }

        private IEnumerable<ILocation> _locations;
        public IEnumerable<ILocation> Locations
        {
            get => _locations;
            set
            {
                if (value != null)
                {
                    _locations = value;
                    RaisePropertyChanged(() => Locations);
                }
            }
        }

        private ILocation _selectedLocation;
        public ILocation SelectedLocation
        {
            get => _selectedLocation;
            set
            {
                if (value != null)
                {
                    _selectedLocation = value;
                    RaisePropertyChanged(() => SelectedLocation);
                }
            }
        }

        private IMvxCommand _goBack;
        public IMvxCommand GoBack
        {
            get
            {
                if (_goBack == null)
                {
                    _goBack = new MvxCommand(() => 
                    {
                        _navigationService.Navigate<MainMenuViewModel>();
                    });
                }
                return _goBack;
            }
        }

        private IMvxCommand _continueToVehicleSelection;
        public IMvxCommand ContinueToVehicleSelection
        {
            get
            {
                if (_continueToVehicleSelection == null)
                {
                    _continueToVehicleSelection = new MvxAsyncCommand(() => (Task.Run(() => GoToVehicleSelection())));
                }
                return _continueToVehicleSelection;
            }
        }

        // TODO
        // This can be (should be!) done better!
        private void GoToVehicleSelection()
        {
            DateTime fromDate, toDate;
            bool fromDateValidity, toDateValidity;
            (fromDate, fromDateValidity) = ConstructTime(FromDate, FromTime);
            (toDate, toDateValidity) = ConstructTime(ToDate, ToTime);

            if (!ValidateFields((message) => Message = message))
            {
                
            }
            else if (!CheckValidityOfTimes(fromDate, toDate, fromDateValidity, toDateValidity, (message) => Message = message))
            {
                
            }
            else
            {
                var order = Mvx.IoCProvider.IoCConstruct<Order>();
                order.UserId = _userService.CurrentUser.UserId;
                order.LocationId = SelectedLocation.LocationId;
                order.FromDate = fromDate;
                order.ToDate = toDate;
                _orderService.CurrentOrder = order;
                _navigationService.Navigate<RentingViewModel>();
            }
        }

        private bool ValidateFields(Action<string> giveMessage)
        {
            if (string.IsNullOrWhiteSpace(FromDate) || string.IsNullOrWhiteSpace(FromTime) 
                || string.IsNullOrWhiteSpace(ToDate) || string.IsNullOrWhiteSpace(ToTime))
            {
                giveMessage("Fill all textboxes!");
                return false;
            }
            else if (SelectedLocation == null)
            {
                giveMessage("Select location!");
                return false;
            }
            else
            {
                return true;
            }
        }

        private (DateTime, bool) ConstructTime(string date, string time)
        {
            bool validity = DateTime.TryParse($"{date} {time}", out DateTime dateTime);
            return (dateTime, validity);
        }


        private bool CheckValidityOfTimes(DateTime from, DateTime to, bool fromValidity, bool toValidity, Action<string> giveMessage)
        {
            bool validity = false;
            if (!fromValidity)
            {
                giveMessage("Starting date is invalid!");
            }
            else if (!toValidity)
            {
                giveMessage("Ending date is invalid!");
            }
            else if (from > to)
            {
                giveMessage("Renting date has to be before returning day!");
            }
            // TODO
            // Here can be problems! (Different time zones.)
            else if (from < DateTime.Now)
            {
                giveMessage("Renting date has to be in future!");
            }
            else
            {
                validity = true;
            }
            return validity;
        }
    }
}
