﻿using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using System;
using System.Threading.Tasks;
using TransportRenting.Core.Services;

namespace TransportRenting.Core.ViewModels
{
    class ConfirmationScreenViewModel : MvxViewModel
    {
        readonly IMvxNavigationService _navigationService;
        public ConfirmationScreenViewModel(IMvxNavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public override async Task Initialize()
        {
            await base.Initialize();
        }

        private IMvxCommand _goMainMenu;
        public IMvxCommand GoMainMenu
        {
            get
            {
                if (_goMainMenu == null)
                {
                    _goMainMenu = new MvxCommand(() => GoBackToMainPage());
                }
                return _goMainMenu;
            }
        }

        private void GoBackToMainPage()
        {
            _navigationService.Navigate<MainMenuViewModel>();
        }
    }
}
