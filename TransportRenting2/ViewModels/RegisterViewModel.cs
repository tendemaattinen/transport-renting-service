﻿using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using System;
using System.Threading.Tasks;
using TransportRenting.Core.Models;
using TransportRenting.Core.Services;

namespace TransportRenting.Core.ViewModels
{
    public class RegisterViewModel : MvxViewModel
    {
        readonly IMvxNavigationService _navigationService;
        readonly IValidationService _validationService;
        readonly IDatabaseService _databaseService;
        readonly IPasswordService _passwordService;
        public RegisterViewModel(IMvxNavigationService navigationService, IValidationService validationService, IDatabaseService databaseService, IPasswordService passwordService)
        {
            _navigationService = navigationService;
            _validationService = validationService;
            _databaseService = databaseService;
            _passwordService = passwordService;
        }

        public override async Task Initialize()
        {
            await base.Initialize();
        }

        private string _username;
        public string Username
        {
            get => _username;
            set
            {
                if (value != null)
                {
                    _username = value;
                    RaisePropertyChanged(() => Username);
                }
            }
        }

        private string _email;
        public string Email
        {
            get => _email;
            set
            {
                if (value != null)
                {
                    _email = value;
                    RaisePropertyChanged(() => Email);
                }
            }
        }

        private string _firstName;
        public string FirstName
        {
            get => _firstName;
            set
            {
                if (value != null)
                {
                    _firstName = value;
                    RaisePropertyChanged(() => FirstName);
                }
            }
        }

        private string _lastName;
        public string LastName
        {
            get => _lastName;
            set
            {
                if (value != null)
                {
                    _lastName = value;
                    RaisePropertyChanged(() => LastName);
                }
            }
        }

        private string _password;
        public string Password
        {
            get => _password;
            set
            {
                if (value != null)
                {
                    _password = value;
                    RaisePropertyChanged(() => Password);
                }
            }
        }

        private string _passwordConfirm;
        public string PasswordConfirm
        {
            get => _passwordConfirm;
            set
            {
                if (value != null)
                {
                    _passwordConfirm = value;
                    RaisePropertyChanged(() => PasswordConfirm);
                }
            }
        }

        private string _messageVisibility = "Collapsed";
        public string MessageVisibility
        {
            get => _messageVisibility;
            set
            {
                if (value != null)
                {
                    _messageVisibility = value;
                    RaisePropertyChanged(() => MessageVisibility);
                }
            }
        }

        private string _message;
        public string Message
        {
            get => _message;
            set
            {
                if (value != null)
                {
                    _message = value;
                    RaisePropertyChanged(() => Message);
                    MessageVisibility = "Visible";
                }
            }
        }

        private DateTime _birthdate;
        public DateTime Birthdate
        {
            get => _birthdate;
            set
            {
                if (value != null)
                {
                    _birthdate = value;
                    RaisePropertyChanged(() => Birthdate);
                }
            }
        }

        private IMvxCommand _register;
        public IMvxCommand Register
        {
            get
            {
                if (_register == null)
                {
                    _register = new MvxAsyncCommand(() => (Task.Run(() => RegisterNewAccount())));
                }
                return _register;
            }
        }

        private void RegisterNewAccount()
        {
            string name = Username;
            Guid userId = Guid.NewGuid();
            string firstName = FirstName;
            string lastName = LastName;
            string email = Email;
            DateTime birthdate = Birthdate;
            var args = new { name, userId, firstName, lastName, email, birthdate };
            User user = Mvx.IoCProvider.IoCConstruct<User>(args);

            if (_validationService.ValidateRegistration(user, Password, PasswordConfirm, (message) => Message = message))
            {
                if (_databaseService.AddNewAccountToDatabase(user, _passwordService.GenerateHash(Password), (message) => Message = message))
                {
                    Message = "Registration was successful!";
                    EmptyTextBoxes();
                    EmptyPasswordBoxes();
                }
                else
                {
                    EmptyPasswordBoxes();
                }
            }
            else
            {
                EmptyPasswordBoxes();
            }
        }

        private IMvxCommand _goBack;
        public IMvxCommand GoBack
        {
            get
            {
                if (_goBack == null)
                {
                    _goBack = new MvxCommand(() =>
                    {
                        _navigationService.Navigate<LoginViewModel>();
                    });
                }
                return _goBack;
            }
        }

        private void EmptyTextBoxes()
        {
            Username = string.Empty;
            Email = string.Empty;
            FirstName = string.Empty;
            LastName = string.Empty;
        }

        private void EmptyPasswordBoxes()
        {
            Password = string.Empty;
            PasswordConfirm = string.Empty;
        }
    }
}
