﻿using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using System;
using System.Threading.Tasks;
using TransportRenting.Core.Services;

namespace TransportRenting.Core.ViewModels
{
    class ConfirmationViewModel : MvxViewModel
    {
        readonly IMvxNavigationService _navigationService;
        readonly IDatabaseService _databaseService;
        readonly IOrderService _orderService;
        readonly IPriceService _priceService;
        public ConfirmationViewModel(IMvxNavigationService navigationService, IDatabaseService databaseService,
            IOrderService orderService, IPriceService priceService)
        {
            _navigationService = navigationService;
            _databaseService = databaseService;
            _orderService = orderService;
            _priceService = priceService;
        }

        public override async Task Initialize()
        {
            await base.Initialize();

            StartDate = _orderService.CurrentOrder.FromDate.ToString("dd.MM.yyyy");
            EndDate = _orderService.CurrentOrder.ToDate.ToString("dd.MM.yyyy");

            VehicleInfo = $"{_orderService.CurrentOrder.Vehicle.Maker} {_orderService.CurrentOrder.Vehicle.Model} ({_orderService.CurrentOrder.Vehicle.Year})";

            var price = Math.Ceiling(await Task.Run(() => _priceService
                .CalculatePrice(_orderService.CurrentOrder.Vehicle.BasePrice, _orderService.CurrentOrder.FromDate, _orderService.CurrentOrder.ToDate)));
            _orderService.CurrentOrder.Price = price;
            Price = $"{price}€";
        }

        private string _price;
        public string Price
        {
            get => _price;
            set
            {
                if (value != null)
                {
                _price = value;
                RaisePropertyChanged(() => Price);
                }
            }
        }

        private string _startDate;
        public string StartDate
        {
            get => _startDate;
            set
            {
                if (value != null)
                {
                    _startDate = value;
                    RaisePropertyChanged(() => StartDate);
                }
            }
        }

        private string _endDate;
        public string EndDate
        {
            get => _endDate;
            set
            {
                if (value != null)
                {
                    _endDate = value;
                    RaisePropertyChanged(() => EndDate);
                }
            }
        }

        private string _vehicleInfo;
        public string VehicleInfo
        {
            get => _vehicleInfo;
            set
            {
                if (value != null)
                {
                    _vehicleInfo = value;
                    RaisePropertyChanged(() => VehicleInfo);
                }
            }
        }

        private string _messageVisibility = "Collapsed";
        public string MessageVisibility
        {
            get => _messageVisibility;
            set
            {
                if (value != null)
                {
                    _messageVisibility = value;
                    RaisePropertyChanged(() => MessageVisibility);
                }
            }
        }

        private string _message;
        public string Message
        {
            get => _message;
            set
            {
                if (value != null)
                {
                    _message = value;
                    RaisePropertyChanged(() => Message);
                }
            }
        }

        private string _discountCode;
        public string DiscountCode
        {
            get => _discountCode;
            set
            {
                if (value != null)
                {
                    _discountCode = value;
                    RaisePropertyChanged(() => DiscountCode);
                }
            }
        }

        private IMvxCommand _enterDiscountCode;
        public IMvxCommand EnterDiscountCode
        {
            get
            {
                if (_enterDiscountCode == null)
                {
                    _enterDiscountCode = new MvxAsyncCommand(() => Task.Run(() => CalculateDiscountedPrice()));
                }
                return _enterDiscountCode;
            }
        }

        private void CalculateDiscountedPrice()
        {
            if (string.IsNullOrWhiteSpace(DiscountCode))
            {
                MessageVisibility = "Visible";
                Message = "Invalid code!";
            }
            else
            {
                var price = Math.Ceiling(_priceService
                .CalculatePrice(_orderService.CurrentOrder.Vehicle.BasePrice, _orderService.CurrentOrder.FromDate, _orderService.CurrentOrder.ToDate, DiscountCode));
                _orderService.CurrentOrder.Price = price;
                Price = $"{price}€";
            }
        }

        private IMvxCommand _goBack;
        public IMvxCommand GoBack
        {
            get
            {
                if (_goBack == null)
                {
                    _goBack = new MvxCommand(() =>
                    {
                        _orderService.CurrentOrder.Vehicle = null;
                        _orderService.CurrentOrder.VehicleId = Guid.NewGuid();
                        _navigationService.Navigate<RentingViewModel>();
                    }
                    );
                }
                return _goBack;
            }
        }

        private IMvxCommand _confirmOrder;
        public IMvxCommand ConfirmOrder
        {
            get
            {
                if (_confirmOrder == null)
                {
                    _confirmOrder = new MvxAsyncCommand(() => (Task.Run(() => AddOrderToDatabase())));
                }
                return _confirmOrder;
            }
        }

        private void AddOrderToDatabase()
        {
            MessageVisibility = "Visible";
            if (_databaseService.AddNewOrderToDatabase(_orderService.CurrentOrder, (message) => Message = message))
            {
                _navigationService.Navigate<ConfirmationScreenViewModel>();
            }
        }
    }
}
