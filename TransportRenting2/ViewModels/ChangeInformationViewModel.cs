﻿using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TransportRenting.Core.Models;
using TransportRenting.Core.Services;
using System.ComponentModel.DataAnnotations;

namespace TransportRenting.Core.ViewModels
{
    class ChangeInformationViewModel : MvxViewModel
    {
        readonly IMvxNavigationService _navigationService;
        readonly IDatabaseService _databaseService;
        public ChangeInformationViewModel(IMvxNavigationService navigationService, IDatabaseService databaseService)
        {
            _navigationService = navigationService;
            _databaseService = databaseService;
        }

        public override async Task Initialize()
        {
            await base.Initialize();
            DateTime.TryParse("1.1.0001", out DateTime date);
            Birthdate = date;
        }

        private string _username;
        public string Username
        {
            get => _username;
            set
            {
                if (value != null)
                {
                    _username = value;
                    RaisePropertyChanged(() => Username);
                }
            }
        }

        private string _email;
        public string Email
        {
            get => _email;
            set
            {
                if (value != null)
                {
                    _email = value;
                    RaisePropertyChanged(() => Email);
                }
            }
        }

        private string _firstName;
        public string FirstName
        {
            get => _firstName;
            set
            {
                if (value != null)
                {
                    _firstName = value;
                    RaisePropertyChanged(() => FirstName);
                }
            }
        }

        private string _lastName;
        public string LastName
        {
            get => _lastName;
            set
            {
                if (value != null)
                {
                    _lastName = value;
                    RaisePropertyChanged(() => LastName);
                }
            }
        }

        private IMvxCommand _goBack;
        public IMvxCommand GoBack
        {
            get
            {
                if (_goBack == null)
                {
                    _goBack = new MvxCommand(() =>
                    {
                        _navigationService.Navigate<SettingsViewModel>();
                    });
                }
                return _goBack;
            }
        }

        private string _messageVisibility = "Collapsed";
        public string MessageVisibility
        {
            get => _messageVisibility;
            set
            {
                if (value != null)
                {
                    _messageVisibility = value;
                    RaisePropertyChanged(() => MessageVisibility);
                }
            }
        }

        private string _message;
        public string Message
        {
            get => _message;
            set
            {
                if (value != null)
                {
                    _message = value;
                    RaisePropertyChanged(() => Message);
                    MessageVisibility = "Visible";
                }
            }
        }

        private DateTime _birthdate;
        public DateTime Birthdate
        {
            get => _birthdate;
            set
            {
                if (value != null)
                {
                    _birthdate = value;
                    RaisePropertyChanged(() => Birthdate);
                }
            }
        }

        private IMvxCommand _changeInformation;
        public IMvxCommand ChangeInformation
        {
            get
            {
                if (_changeInformation == null)
                {
                    _changeInformation = new MvxCommand(() => ChangeUsersInformation());
                }
                return _changeInformation;
            }
        }

        public void ChangeUsersInformation()
        {
            // TODO
            // Better validation and messages to end user.
            MessageVisibility = "Visible";
            DateTime.TryParse("1.1.0001", out DateTime date);
            if (!(Birthdate == date))
            {
                _databaseService.ChangeBirthdate(Birthdate);
            }
            if ((Username.Length >= 5) && (Username.Length <= 50) && (!_databaseService.CheckIfUserExistsInDatabase(Username)))
            {
                _databaseService.ChangeUsername(Username);
            }
            else
            {
                Console.WriteLine("Invalid username");
            }
            if (!(string.IsNullOrWhiteSpace(FirstName)))
            {
                _databaseService.ChangeFirstName(FirstName);
            }
            if (!(string.IsNullOrWhiteSpace(LastName)))
            {
                _databaseService.ChangeLastName(LastName);
            }
            if (!(string.IsNullOrWhiteSpace(Email)))
            {
                if (new EmailAddressAttribute().IsValid(Email))
                {
                    _databaseService.ChangeEmail(Email);
                }
                else
                {
                    Console.WriteLine("Invalid email.");
                }
            }
            Message = "Information updated.";
            EmptyTextBoxes();
        }

        private void EmptyTextBoxes()
        {
            Username = string.Empty;
            Email = string.Empty;
            FirstName = string.Empty;
            LastName = string.Empty;
        }

    }
}
