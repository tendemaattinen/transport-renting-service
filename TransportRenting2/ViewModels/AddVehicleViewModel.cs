﻿using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TransportRenting.Core.Models;
using TransportRenting.Core.Services;

namespace TransportRenting.Core.ViewModels
{
    class AddVehicleViewModel : MvxViewModel
    {
        readonly IMvxNavigationService _navigationService;
        readonly IDatabaseService _databaseService;

        public AddVehicleViewModel(IMvxNavigationService navigationService, IDatabaseService databaseService)
        {
            _navigationService = navigationService;
            _databaseService = databaseService;
        }
        public override async Task Initialize()
        {
            await base.Initialize();

            Locations = await Task.Run(() => _databaseService.GetLocations());
        }

        private IEnumerable<ILocation> _locations;
        public IEnumerable<ILocation> Locations
        {
            get => _locations;
            set
            {
                if (value != null)
                {
                    _locations = value;
                    RaisePropertyChanged(() => Locations);
                }
            }
        }

        private ILocation _selectedLocation;
        public ILocation SelectedLocation
        {
            get => _selectedLocation;
            set
            {
                if (value != null)
                {
                    _selectedLocation = value;
                    RaisePropertyChanged(() => SelectedLocation);
                }
            }
        }

        private string _messageVisibility = "Collapsed";
        public string MessageVisibility
        {
            get => _messageVisibility;
            set
            {
                if (value != null)
                {
                    _messageVisibility = value;
                    RaisePropertyChanged(() => MessageVisibility);
                }
            }
        }

        private string _message;
        public string Message
        {
            get => _message;
            set
            {
                if (value != null)
                {
                    _message = value;
                    RaisePropertyChanged(() => Message);
                    MessageVisibility = "Visible";
                }
            }
        }

        private string _maker;
        public string Maker
        {
            get => _maker;
            set
            {
                if (value != null)
                {
                    _maker = value;
                    RaisePropertyChanged(() => Maker);
                }
            }
        }

        private string _model;
        public string Model
        {
            get => _model;
            set
            {
                if (value != null)
                {
                    _model = value;
                    RaisePropertyChanged(() => Model);
                }
            }
        }

        private string _year;
        public string Year
        {
            get => _year;
            set
            {
                _year = value;
                RaisePropertyChanged(() => Year);
            }
        }

        private string _type;
        public string Type
        {
            get => _type;
            set
            {
                if (value != null)
                {
                    _type = value;
                    RaisePropertyChanged(() => Type);
                }
            }
        }

        private string _basePrice;
        public string BasePrice
        {
            get => _basePrice;
            set
            {
                _basePrice = value;
                RaisePropertyChanged(() => BasePrice);
            }
        }


        private IMvxCommand _goBack;
        public IMvxCommand GoBack
        {
            get
            {
                if (_goBack == null)
                {
                    _goBack = new MvxCommand(() =>
                    {
                        _navigationService.Navigate<SettingsViewModel>();
                    });
                }
                return _goBack;
            }
        }

        private IMvxCommand _addNewVehicle;
        public IMvxCommand AddNewVehicle
        {
            get
            {
                if (_addNewVehicle == null)
                {
                    _addNewVehicle = new MvxCommand(() => AddNewVehicleToDatabase());
                }
                return _addNewVehicle;
            }
        }

        // TODO
        // This can be written more beautifully.
        private void AddNewVehicleToDatabase()
        {
            var yearValidity = int.TryParse(Year, out int year);
            BasePrice = BasePrice.Replace(".", ",");
            var basePriceValidity = double.TryParse(BasePrice, out double basePrice);

            if (yearValidity && basePriceValidity && (!string.IsNullOrWhiteSpace(Maker))
                && (!string.IsNullOrWhiteSpace(Model)) && (!string.IsNullOrWhiteSpace(Type))
                && (SelectedLocation != null))
            {
                var vehicle = Mvx.IoCProvider.IoCConstruct<Vehicle>();
                vehicle.Maker = Maker;
                vehicle.Model = Model;
                vehicle.Year = year;
                vehicle.Type = Type;
                vehicle.BasePrice = basePrice;
                vehicle.LocationId = SelectedLocation.LocationId;
                _databaseService.AddVehicleToDataBase(vehicle, (message) => Message = message);
                EmptyTextBoxes();
            }
            else
            {
                Message = "Invalid parameters";
            }
        }

        private void EmptyTextBoxes()
        {
            Maker = string.Empty;
            Model = string.Empty;
            Year = string.Empty;
            Type = string.Empty;
            BasePrice = string.Empty;
        }
    }
}
