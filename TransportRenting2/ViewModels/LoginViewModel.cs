﻿using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using System.Threading.Tasks;
using TransportRenting.Core.Services;

namespace TransportRenting.Core.ViewModels
{
    public class LoginViewModel : MvxViewModel
    {
        readonly IMvxNavigationService _navigationService;
        readonly ILoginService _loginService;
        public LoginViewModel(IMvxNavigationService navigationService, ILoginService loginService)
        {
            _navigationService = navigationService;
            _loginService = loginService;
        }

        public override async Task Initialize()
        {
            await base.Initialize();

            // TODO
            // Remove!
            // For easier testing.
            Username = "teemu";
            Password = "12345";
        }

        private string _username;
        public string Username
        {
            get => _username;
            set
            {
                if (value != null)
                {
                    _username = value;
                    RaisePropertyChanged(() => Username);
                }
            }
        }

        private string _password;
        public string  Password
        {   
            get => _password;
            set
            {
                if (value != null)
                {
                    _password = value;
                    RaisePropertyChanged(() => Password);
                }
            }
        }

        private string _messageVisibility = "Hidden";
        public string MessageVisibility
        {
            get => _messageVisibility;
            set
            {
                if (value != null)
                {
                    _messageVisibility = value;
                    RaisePropertyChanged(() => MessageVisibility);
                }
            }
        }

        private string _message;
        public string Message
        {
            get => _message;
            set
            {
                if (value != null)
                {
                    _message = value;
                    RaisePropertyChanged(() => Message);
                    MessageVisibility = "Visible";
                }
            }
        }

        private IMvxCommand _login;
        public IMvxCommand Login
        {
            get
            {
                if (_login == null)
                {
                    _login = new MvxAsyncCommand(() => (Task.Run(() => LoginToApplication())));
                }
                return _login;
            }
        }
        public void LoginToApplication()
        {
            if (_loginService.Login(Username, Password, (message => Message = message)))
            {
                _navigationService.Navigate<MainMenuViewModel>();
            }
            else
            {
                Password = string.Empty;
            }
        }

        private IMvxCommand _registerView;
        public IMvxCommand RegisterView
        {
            get
            {
                if (_registerView == null)
                {
                    _registerView = new MvxCommand(() => 
                    {
                        _navigationService.Navigate<RegisterViewModel>();
                    });
                }
                return _registerView;
            }
        }
    }
}
