﻿using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using System.Threading.Tasks;
using System.Windows.Input;
using TransportRenting.Core.Models;
using TransportRenting.Core.Services;

namespace TransportRenting.Core.ViewModels
{
    class MainMenuViewModel : MvxViewModel
    {
        readonly IMvxNavigationService _navigationService;
        readonly IUserService _userService;
        readonly IDatabaseService _databaseService;
        public MainMenuViewModel(IMvxNavigationService navigationService, IUserService userService, IDatabaseService databaseService)
        {
            _navigationService = navigationService;
            _userService = userService;
            _databaseService = databaseService;
        }

        public override async Task Initialize()
        {
            await base.Initialize();
        }

        private IMvxCommand _logout;
        public IMvxCommand Logout
        {
            get
            {
                if (_logout == null)
                {
                    _logout = new MvxCommand(() => LogoutFromApp());
                }
                return _logout;
            }
        }

        private void LogoutFromApp()
        {
            _userService.LogUserOut();
            _navigationService.Navigate<LoginViewModel>();
        }

        private IMvxCommand _rent;
        public IMvxCommand Rent
        {
            get
            {
                if (_rent == null)
                {
                    _rent = new MvxCommand(() => RentVehicle());
                }
                return _rent;
            }
        }

        private void RentVehicle()
        {
            _navigationService.Navigate<DateAndLocationViewModel>();
        }

        private IMvxCommand _viewOrders;
        public IMvxCommand ViewOrders
        {
            get
            {
                if (_viewOrders == null)
                {
                    _viewOrders = new MvxCommand(() => ViewUsersOrders());
                }
                return _viewOrders;
            }
        }

        private void ViewUsersOrders()
        {
            _navigationService.Navigate<UsersOrdersViewModel>();
        }

        private IMvxCommand _viewLocations;
        public IMvxCommand ViewLocations
        {
            get
            {
                if (_viewLocations == null)
                {
                    _viewLocations = new MvxCommand(() => ViewAllLocations());
                }
                return _viewLocations;
            }
        }

        private void ViewAllLocations()
        {
            // TODO
            // Go to locations.

        }

        private IMvxCommand _settings;
        public IMvxCommand Settings
        {
            get
            {
                if (_settings == null)
                {
                    //_settings = new MvxAsyncCommand(() => Task.Run(() => _databaseService.CreatePlaceholderDatabase()));
                    _settings = new MvxCommand(() => 
                    {
                        _navigationService.Navigate<SettingsViewModel>();
                    });
                }
                return _settings;
            }
        }
    }
}
