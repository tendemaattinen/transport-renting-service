﻿using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;
using TransportRenting.Core.Models;
using TransportRenting.Core.Services;

namespace TransportRenting.Core.ViewModels
{
    class RentingViewModel : MvxViewModel
    {
        readonly IMvxNavigationService _navigationService;
        readonly IDatabaseService _databaseService;
        readonly IOrderService _orderService;
        public RentingViewModel(IMvxNavigationService navigationService, IDatabaseService databaseService, IOrderService orderService)
        {
            _navigationService = navigationService;
            _databaseService = databaseService;
            _orderService = orderService;
        }

        public override async Task Initialize()
        {
            await base.Initialize();
            Vehicles = await Task.Run(() => _databaseService.GetLocationsVehiclesFromDatabase(_orderService.CurrentOrder.LocationId));
        }

        private IEnumerable<IVehicle> _vehicles; 
        public IEnumerable<IVehicle> Vehicles
        {
            get => _vehicles;
            set
            {
                if (value != null)
                {
                    _vehicles = value;
                    RaisePropertyChanged(() => Vehicles);
                }
            }
        }

        private IVehicle _selecteVehicle;
        public IVehicle SelectedVehicle
        {
            get => _selecteVehicle;
            set
            {
                if (value != null)
                {
                    _selecteVehicle = value;
                    RaisePropertyChanged(() => SelectedVehicle);
                }
            }
        }

        private string _message;
        public string Message
        {
            get => _message;
            set
            {
                if (value != null)
                {
                    _message = value;
                    RaisePropertyChanged(() => Message);
                }
            }
        }

        private IMvxCommand _rent;
        public IMvxCommand Rent
        {
            get
            {
                if (_rent == null)
                {
                    _rent = new MvxAsyncCommand(() => (Task.Run(() => RentAVehicle())));
                }
                return _rent;
            }
        }

        private void RentAVehicle()
        {
            if (SelectedVehicle != null)
            {
                _orderService.CurrentOrder.VehicleId = SelectedVehicle.VehicleId;
                _orderService.CurrentOrder.Vehicle = SelectedVehicle;
                _navigationService.Navigate<ConfirmationViewModel>();
            }
            else
            {
                Message = "Select a vehicle!";
            }
        }

        private IMvxCommand _goBack;
        public IMvxCommand GoBack
        {
            get
            {
                if (_goBack == null)
                {
                    _goBack = new MvxCommand(() =>
                    {
                        _orderService.CurrentOrder = null;
                        _navigationService.Navigate<DateAndLocationViewModel>();
                    });
                }
                return _goBack;
            }
        }
    }
}
