﻿using TransportRenting.Core.Models;
using System;

namespace TransportRenting.Core.Utilities
{
    public interface IAgeValidation
    {
        bool ValidateAge(IUser user, DateTime timeNow, int ageLimit);
    }
}