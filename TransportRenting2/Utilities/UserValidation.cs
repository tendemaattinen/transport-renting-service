﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using TransportRenting.Core.Models;

namespace TransportRenting.Core.Utilities
{
    public class UserValidation : AbstractValidator<User>
    {
        public UserValidation(int usernameMinLength, int usernameMaxLength)
        {
            RuleFor(user => user.Username).NotNull().WithMessage("Username can't be empty!")
                .MaximumLength(usernameMaxLength).WithMessage($"Maximum length of username is {usernameMaxLength} characters!")
                .MinimumLength(usernameMinLength).WithMessage($"Minimum length of username is {usernameMinLength} characters!")
                .NotEmpty().WithMessage("Username can't be empty!");
            RuleFor(user => user.Email).NotNull().WithMessage("Email can't be empty!")
                .EmailAddress().WithMessage("Email is not valid!")
                .NotEmpty().WithMessage("Email can't be empty!");
            RuleFor(user => user.FirstName).NotNull().WithMessage("First name can't be empty!")
                .NotEmpty().WithMessage("First name can't be empty!");
            RuleFor(user => user.LastName).NotNull().WithMessage("Last name can't be empty!")
                .NotEmpty().WithMessage("Last name can't be empty!");
            RuleFor(user => user.Birthdate).NotNull().WithMessage("Date can't be empty!");
        }
    }
}
