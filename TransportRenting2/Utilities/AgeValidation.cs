﻿using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using TransportRenting.Core.Models;
using TransportRenting.Core.Services;

namespace TransportRenting.Core.Utilities
{
    public class AgeValidation : IAgeValidation
    {
        public bool ValidateAge(IUser user, DateTime timeNow, int ageLimit)
        {
            int yearNow = timeNow.Year;
            int yearBirth = user.Birthdate.Year;

            if ((yearNow - yearBirth) > ageLimit)
            {
                return true;
            }
            else if ((yearNow - yearBirth) < ageLimit)
            {
                return false;
            }
            else
            {
                int monthValidity = user.Birthdate.Month.CompareTo(timeNow.Month);
                if (monthValidity == -1)
                {
                    return true;
                }
                else if (monthValidity == 1)
                {
                    return false;
                }
                else
                {
                    int dayValidity = user.Birthdate.Day.CompareTo(timeNow.Day);
                    if (dayValidity == 1)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
        }
    }
}
