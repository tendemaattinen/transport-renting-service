﻿using System;

namespace TransportRenting.Core.Models
{
    public interface IVehicle
    {
        string Maker { get; set; }
        string Model { get; set; }
        string Type { get; set; }
        Guid VehicleId { get; set; }
        int Year { get; set; }
        double BasePrice { get; set; }
        Guid LocationId { get; set; }
        string LocationName { get; set; }

        void CreateVehicle(Guid id, string maker, string model, int year, string type, double price, string locationName, Guid locationId);
    }
}