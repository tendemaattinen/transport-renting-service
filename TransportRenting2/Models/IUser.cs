﻿using System;

namespace TransportRenting.Core.Models
{
    public interface IUser
    {
        string Email { get; set; }
        string FirstName { get; set; }
        string FullName { get; }
        bool IsAdmin { get; set; }
        string LastName { get; set; }
        Guid UserId { get; set; }
        string Username { get; set; }
        DateTime Birthdate { get; set; }
    }
}