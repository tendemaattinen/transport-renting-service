﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransportRenting.Core.Models
{
    public class User : IUser
    {
        public string Username { get; set; }
        public Guid UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime Birthdate { get; set; }
        public string FullName
        {
            get => (FirstName + " " + LastName);
        }
        public bool IsAdmin { get; set; }

        public User(string name, Guid userId, string firstName, string lastName, string email, DateTime birthdate, bool isAdmin = false)
        {
            Username = name;
            UserId = userId;
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            Birthdate = birthdate;
            IsAdmin = isAdmin;
        }

        public User()
        {

        }
    }
}
