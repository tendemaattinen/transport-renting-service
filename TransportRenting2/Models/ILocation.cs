﻿using System;

namespace TransportRenting.Core.Models
{
    public interface ILocation
    {
        Guid LocationId { get; set; }
        string LocationName { get; set; }

        void CreateLocation(Guid id, string name);
    }
}