﻿using System;

namespace TransportRenting.Core.Models
{
    public interface IOrder
    {
        DateTime FromDate { get; set; }
        Guid OrderId { get; set; }
        DateTime ToDate { get; set; }
        Guid UserId { get; set; }
        Guid VehicleId { get; set; }
        Guid LocationId { get; set; }
        IVehicle Vehicle { get; set; }
        double Price { get; set; }
        void CreateOrder(Guid orderId, Guid vehicleId, Guid userId, Guid locationId, DateTime fromDate, DateTime toDate, double price);
    }
}