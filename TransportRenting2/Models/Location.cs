﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransportRenting.Core.Models
{
    public class Location : ILocation
    {
        public Guid LocationId { get; set; }
        public string LocationName { get; set; }

        public void CreateLocation(Guid id, string name)
        {
            LocationId = id;
            LocationName = name;
        }
    }
}
