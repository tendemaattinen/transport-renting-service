﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransportRenting.Core.Models
{
    public class Order : IOrder
    {
        public Guid OrderId { get; set; }
        public Guid VehicleId { get; set; }
        public Guid UserId { get; set; }
        public Guid LocationId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public IVehicle Vehicle { get; set; }
        public double Price { get; set; }

        // TODO
        // Change vehicleId to Vehicle object?
        public void CreateOrder(Guid orderId, Guid vehicleId, Guid userId, Guid locationId, DateTime fromDate, DateTime toDate, double price)
        {
            OrderId = orderId;
            VehicleId = vehicleId;
            UserId = userId;
            LocationId = locationId;
            FromDate = fromDate;
            ToDate = toDate;
            Price = price;
        }
    }
}
