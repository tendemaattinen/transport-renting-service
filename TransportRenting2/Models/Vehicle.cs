﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransportRenting.Core.Models
{
    public class Vehicle : IVehicle
    {
        public Guid VehicleId { get; set; }
        public string Maker { get; set; }
        public string Model { get; set; }
        public int Year { get; set; }
        public string Type { get; set; }
        public double BasePrice { get; set; }
        public Guid LocationId { get; set; }
        public string LocationName { get; set; }

        public void CreateVehicle(Guid id, string maker, string model, int year, string type, double price, string locationName, Guid locationId)
        {
            VehicleId = id;
            Maker = maker;
            Model = model;
            Year = year;
            Type = type;
            BasePrice = price;
            LocationName = locationName;
            LocationId = locationId;
        }
    }
}
