﻿using System;
using TransportRenting.Core.Models;

namespace TransportRenting.Core.Services
{
    public interface IValidationService
    {
        bool CheckPasswordValidity(string password, string password2, int minLenght, int maxLenght, Action<string> giveMessage);
        bool ValidateRegistration(User user, string password, string password2, Action<string> giveMessage);
    }
}