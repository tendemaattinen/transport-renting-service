﻿using System;
using System.Collections.Generic;
using TransportRenting.Core.Models;
using System.Threading.Tasks;

namespace TransportRenting.Core.Services
{
    public interface IDatabaseService
    {
        bool AddNewAccountToDatabase(User user, string hashedPassword, Action<string> giveMessage);
        bool CheckIfUserExistsInDatabase(string username);
        IEnumerable<ILocation> GetLocations();
        IEnumerable<IVehicle> GetLocationsVehiclesFromDatabase(Guid locationId);
        User GetUserFromDatabase(string username, string password, Action<string> giveMessage);
        IEnumerable<IVehicle> GetVehiclesFromDatabase();
        bool AddNewOrderToDatabase(IOrder order, Action<string> giveMessage);
        IEnumerable<IOrder> GetOrders();
        void CancelOrder(Guid orderId, Action<string> giveMessage);
        IVehicle GetVehicle(Guid id, Action<string> giveMessage);
        void CreatePlaceholderDatabase();
        void AddLocationToDataBase(string locationName, Action<string> giveMessage);
        void AddVehicleToDataBase(IVehicle vehicle, Action<string> giveMessage);
        bool CheckIfPasswordMatches(string password, Action<string> giveMessage);
        void ChangePassword(string newPassword, Action<string> giveMessage);
        double GetDiscountFactorFromDatabase(int hours);
        double GetDiscountCodeFactorFromDatabase(string code);
        void ChangeUsername(string username);
        void ChangeFirstName(string firstName);
        void ChangeLastName(string lastName);
        void ChangeEmail(string email);
        void ChangeBirthdate(DateTime birthdate);
    }
}