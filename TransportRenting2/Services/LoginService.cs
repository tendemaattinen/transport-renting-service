﻿using MvvmCross;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TransportRenting.Core.Models;

namespace TransportRenting.Core.Services
{
    public class LoginService : ILoginService
    {
        readonly IDatabaseService _databaseService;
        readonly IUserService _userService;
        public LoginService(IDatabaseService databaseService, IUserService userService)
        {
            _databaseService = databaseService;
            _userService = userService;
        }

        public bool Login(string username, string password, Action<string> giveMessage)
        {
            User user;
            try
            {
                user = _databaseService.GetUserFromDatabase(username, password, (message) => giveMessage(message));
                if (user.Username != null)
                {
                    _userService.LogUserIn(user);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                giveMessage(ex.Message);
                return false;
            }
        }
    }
}
