﻿using System;

namespace TransportRenting.Core.Services
{
    public interface ITimeService
    {
        double CalculateNumberOfHours(DateTime startDate, DateTime endDate);
    }
}