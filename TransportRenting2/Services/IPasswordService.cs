﻿namespace TransportRenting.Core.Services
{
    public interface IPasswordService
    {
        string GenerateHash(string hashable);
        bool CheckIfPasswordsMatch(string savedHash, string password);
    }
}