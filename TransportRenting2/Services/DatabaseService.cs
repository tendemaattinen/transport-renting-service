﻿using MvvmCross;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TransportRenting.Core.Models;
using TransportRenting.Core.Database;

namespace TransportRenting.Core.Services
{
    public class DatabaseService : IDatabaseService
    {
        readonly IPasswordService _passwordService;
        readonly IOrderService _orderService;
        readonly IUserService _userService;
        public DatabaseService(IPasswordService passwordService, IOrderService orderService, IUserService userService)
        {
            _passwordService = passwordService;
            _orderService = orderService;
            _userService = userService;
        }

        public bool AddNewAccountToDatabase(User user, string hashedPassword, Action<string> giveMessage)
        {
            try
            {
                using (var context = Mvx.IoCProvider.IoCConstruct<PostgreSqlContext>())
                {
                    context.Database.EnsureCreated();
                    var userDb = new UserDb
                    {
                        Username = user.Username,
                        Email = user.Email,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        IsAdmin = false,
                        Password = hashedPassword,
                        Birthdate = user.Birthdate
                    };
                    context.Users.Add(userDb);
                    context.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                giveMessage(ex.Message);
                return false;
            }
        }

        public User GetUserFromDatabase(string username, string password, Action<string> giveMessage)
        {
            User user = new User();
            try
            {
                using (var context = Mvx.IoCProvider.IoCConstruct<PostgreSqlContext>())
                {
                    context.Database.EnsureCreated();
                    if (!context.Users.Any(u => u.Username == username))
                    {
                        giveMessage("Invalid username/password!");
                    }
                    else
                    {
                        var userDb = context.Users.Single(u => u.Username == username);

                        if (_passwordService.CheckIfPasswordsMatch(userDb.Password, password))
                        {
                            user = new User
                            {
                                Username = userDb.Username,
                                UserId = userDb.UserId,
                                FirstName = userDb.FirstName,
                                LastName = userDb.LastName,
                                Email = userDb.Email,
                                Birthdate = userDb.Birthdate,
                                IsAdmin = userDb.IsAdmin
                            };
                        }
                        else
                        {
                            giveMessage("Invalid username/password!");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                giveMessage(ex.Message);
            }

            return user;
        }

        public bool CheckIfUserExistsInDatabase(string username)
        {
            try
            {
                using (var context = Mvx.IoCProvider.IoCConstruct<PostgreSqlContext>())
                {
                    context.Database.EnsureCreated();
                    var doesUserExists = context.Users.Any(u => u.Username == username);
                    if (doesUserExists)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public IEnumerable<IVehicle> GetVehiclesFromDatabase()
        {
            var vehicleList = new List<VehicleDb>();

            using (var context = Mvx.IoCProvider.IoCConstruct<PostgreSqlContext>())
            {
                vehicleList = context.Vehicles.ToList();
            }

            var vehicleList2 = new List<IVehicle>();

            Parallel.ForEach(vehicleList, vehicle =>
            {
                var veh = Mvx.IoCProvider.IoCConstruct<Vehicle>();
                var locationName = "";

                using (var context = Mvx.IoCProvider.IoCConstruct<PostgreSqlContext>())
                {
                    var location = context.Locations.Single(i => i.LocationId == vehicle.VehicleId);
                    locationName = location.LocationName;
                }

                veh.CreateVehicle(vehicle.VehicleId, vehicle.Maker, vehicle.Model, vehicle.Year, vehicle.Type, vehicle.BasePrice, locationName, vehicle.LocationId);

                vehicleList2.Add(veh);
            });

            return vehicleList2;
        }

        // TODO
        // Less complex structure?
        // Foreach inside foreach loop will be problem when amount of data gets bigger!
        public IEnumerable<IVehicle> GetLocationsVehiclesFromDatabase(Guid locationId)
        {
            var vehicleList = new List<VehicleDb>();
            var orderList = new List<OrderDb>();
            var vehicleList2 = new List<IVehicle>();
            string locationName = null;
            try
            {
                using (var context = Mvx.IoCProvider.IoCConstruct<PostgreSqlContext>())
                {
                    vehicleList = context.Vehicles
                        .Where(i => i.LocationId == locationId)
                        .ToList();

                    orderList = context.Orders
                        .Where(i => i.LocationId == locationId)
                        .ToList();

                    var location = context.Locations
                        .Single(i => i.LocationId == locationId);

                    locationName = location.LocationName;
                }

                foreach (var vehicle in vehicleList)
                {
                    var vehicleIsAvailable = true;
                    foreach (var order in orderList)
                    {
                        if (vehicle.VehicleId == order.VehicleId)
                        {
                            if (_orderService.CurrentOrder.FromDate >= order.FromDate && _orderService.CurrentOrder.ToDate <= order.ToDate)
                            {
                                vehicleIsAvailable = false;
                            }
                            else if (_orderService.CurrentOrder.FromDate <= order.FromDate && _orderService.CurrentOrder.ToDate <= order.ToDate && _orderService.CurrentOrder.ToDate >= order.FromDate)
                            {
                                vehicleIsAvailable = false;
                            }
                            else if (_orderService.CurrentOrder.FromDate >= order.FromDate && _orderService.CurrentOrder.ToDate >= order.ToDate && _orderService.CurrentOrder.FromDate <= order.ToDate)
                            {
                                vehicleIsAvailable = false;
                            }
                            else if (_orderService.CurrentOrder.FromDate <= order.FromDate && _orderService.CurrentOrder.ToDate >= order.ToDate)
                            {
                                vehicleIsAvailable = false;
                            }
                        }
                    }

                    if (vehicleIsAvailable)
                    {
                        var veh = Mvx.IoCProvider.IoCConstruct<Vehicle>();
                        veh.CreateVehicle(vehicle.VehicleId, vehicle.Maker, vehicle.Model, vehicle.Year, vehicle.Type, vehicle.BasePrice, locationName, vehicle.LocationId);
                        vehicleList2.Add(veh);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return vehicleList2;
        }

        public IEnumerable<ILocation> GetLocations()
        {
            var locationList = new List<LocationDb>();

            using (var context = Mvx.IoCProvider.IoCConstruct<PostgreSqlContext>())
            {
                locationList = context.Locations.ToList();
            }

            var locationList2 = new List<ILocation>();

            foreach (var location in locationList)
            {
                var loc = Mvx.IoCProvider.IoCConstruct<Location>();
                loc.CreateLocation(location.LocationId, location.LocationName);
                locationList2.Add(loc);
            }

            return locationList2;
        }

        public bool AddNewOrderToDatabase(IOrder order, Action<string> giveMessage)
        {
            try
            {
                using (var context = Mvx.IoCProvider.IoCConstruct<PostgreSqlContext>())
                {
                    var orderDb = new OrderDb
                    {
                        FromDate = order.FromDate,
                        LocationId = order.LocationId,
                        ToDate = order.ToDate,
                        UserId = order.UserId,
                        VehicleId = order.VehicleId,
                        Price = order.Price
                    };
                    context.Orders.Add(orderDb);
                    context.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                giveMessage(ex.Message);
                return false;
            }
        }

        public IEnumerable<IOrder> GetOrders()
        {
            var orderList = new List<OrderDb>();

            using (var context = Mvx.IoCProvider.IoCConstruct<PostgreSqlContext>())
            {
                orderList = context.Orders
                    .Where(i => i.UserId == _userService.CurrentUser.UserId)
                    .ToList();
            }

            var orderList2 = new List<IOrder>();

            foreach (var order in orderList)
            {
                var newOrder = Mvx.IoCProvider.IoCConstruct<Order>();
                newOrder.CreateOrder(order.OrderId, order.VehicleId, order.UserId, order.LocationId, order.FromDate, order.ToDate, order.Price);
                orderList2.Add(newOrder);
                
            }

            return orderList2;
        }

        public void CancelOrder(Guid orderId, Action<string> giveMessage)
        {
            try
            {
                var orderDb = new OrderDb
                {
                    OrderId = orderId
                };
                using (var context = Mvx.IoCProvider.IoCConstruct<PostgreSqlContext>())
                {
                    context.Orders.Attach(orderDb);
                    context.Orders.Remove(orderDb);
                    context.SaveChanges();
                }
                giveMessage($"Order {orderId} removed successfully!");
            }
            catch (Exception ex)
            {
                giveMessage(ex.Message);
            }
        }

        public IVehicle GetVehicle(Guid id, Action<string> giveMessage)
        {
            IVehicle vehicle = null;
            try
            {
                using (var context = Mvx.IoCProvider.IoCConstruct<PostgreSqlContext>())
                {
                    var vehicleDb = context.Vehicles.Single(veh => veh.VehicleId == id);
                    vehicle = Mvx.IoCProvider.IoCConstruct<Vehicle>();
                    vehicle.Maker = vehicleDb.Maker;
                    vehicle.Model = vehicleDb.Model;
                }
            }
            catch (Exception ex)
            {
                giveMessage(ex.Message);
            }
            return vehicle;
        }

        public ILocation GetLocation(Guid id, Action<string> giveMessage)
        {
            ILocation location = null;
            try
            {
                using (var context = Mvx.IoCProvider.IoCConstruct<PostgreSqlContext>())
                {
                    var locationDb = context.Locations.Single(loc => loc.LocationId == id);
                    location = Mvx.IoCProvider.IoCConstruct<Location>();
                    location.LocationId = locationDb.LocationId;
                    location.LocationName = locationDb.LocationName;
                }
            }
            catch (Exception ex)
            {
                giveMessage(ex.Message);
            }
            return location;
        }

        // TODO
        // Not working as intended right now.
        // Disabled for now.
        // Vehicles location is problem.
        public void CreatePlaceholderDatabase()
        {
            using (var context = Mvx.IoCProvider.IoCConstruct<PostgreSqlContext>())
            {
                context.Database.EnsureCreated();
                for (int i = 0; i < 1000; i++)
                {
                    var userDb = new UserDb
                    {
                        Username = $"nickname{i}",
                        Email = $"{i}@outlook.com",
                        FirstName = $"{i}",
                        LastName = $"{i}nen",
                        IsAdmin = false,
                        Password = _passwordService.GenerateHash("12345"),
                        Birthdate = DateTime.UtcNow
                    };
                    context.Users.Add(userDb);
                }

                context.SaveChanges();

                for (int i = 0; i < 100; i++)
                {
                    var locationDb = new LocationDb
                    {
                        LocationName = $"{i}-location",
                    };
                    context.Locations.Add(locationDb);
                }

                context.SaveChanges();

                Console.WriteLine("Locations ready.");
            }

            using (var context = Mvx.IoCProvider.IoCConstruct<PostgreSqlContext>())
            {
                int k = 1;
                int j = 0;
                for (int i = 0; i < 5000; i++)
                {
                    var vehicleDb = new VehicleDb
                    {
                        Maker = $"Car{i}",
                        Model = $"Model{i}",
                        Year = i,
                        Type = "Land",
                        BasePrice = i * 0.01,
                        LocationId = Guid.NewGuid()
                    };
                    context.Vehicles.Add(vehicleDb);

                    j++;

                    if (j % 100 == 0)
                    {
                        k++;
                    }
                }

                context.SaveChanges();
            }
        }

        public void AddLocationToDataBase(string locationName, Action<string> giveMessage)
        {
            try
            {
                using (var context = Mvx.IoCProvider.IoCConstruct<PostgreSqlContext>())
                {
                    var locationDb = new LocationDb
                    {
                        LocationName = locationName
                    };
                    context.Locations.Add(locationDb);
                    context.SaveChanges();
                }
                giveMessage("Location added!");
            }
            catch (Exception ex)
            {
                giveMessage(ex.Message);
            }
        }

        public void AddVehicleToDataBase(IVehicle vehicle, Action<string> giveMessage)
        {
            try
            {
                using (var context = Mvx.IoCProvider.IoCConstruct<PostgreSqlContext>())
                {
                    var vehicleDb = new VehicleDb
                    {
                        Maker = vehicle.Maker,
                        Model = vehicle.Model,
                        Year = vehicle.Year,
                        Type = vehicle.Type,
                        BasePrice = vehicle.BasePrice,
                        LocationId = vehicle.LocationId
                    };
                    context.Vehicles.Add(vehicleDb);
                    context.SaveChanges();
                }
                giveMessage("Vehicle added!");
            }
            catch (Exception ex)
            {
                giveMessage(ex.Message);
            }
        }

        public bool CheckIfPasswordMatches(string password, Action<string> giveMessage)
        {
            using (var context = Mvx.IoCProvider.IoCConstruct<PostgreSqlContext>())
            {
                var userDb = context.Users.Single(u => u.UserId == _userService.CurrentUser.UserId);
                if (_passwordService.CheckIfPasswordsMatch(userDb.Password, password))
                {
                    return true;
                }
                else
                {
                    giveMessage("Password won't match!");
                    return false;
                }
            }
        }

        public void ChangePassword(string newPassword, Action<string> giveMessage)
        {
            using (var context = Mvx.IoCProvider.IoCConstruct<PostgreSqlContext>())
            {
                var userDb = context.Users.Single(u => u.UserId == _userService.CurrentUser.UserId);
                userDb.Password = _passwordService.GenerateHash(newPassword);
                context.SaveChanges();
            }
            giveMessage("Password changed!");
        }

        public void ChangeUsername(string username)
        {
            using (var context = Mvx.IoCProvider.IoCConstruct<PostgreSqlContext>())
            {
                var userDb = context.Users.Single(u => u.UserId == _userService.CurrentUser.UserId);
                userDb.Username = username;
                context.SaveChanges();
            }
        }

        public void ChangeFirstName(string firstName)
        {
            using (var context = Mvx.IoCProvider.IoCConstruct<PostgreSqlContext>())
            {
                var userDb = context.Users.Single(u => u.UserId == _userService.CurrentUser.UserId);
                userDb.FirstName = firstName;
                context.SaveChanges();
            }
        }

        public void ChangeLastName(string lastName)
        {
            using (var context = Mvx.IoCProvider.IoCConstruct<PostgreSqlContext>())
            {
                var userDb = context.Users.Single(u => u.UserId == _userService.CurrentUser.UserId);
                userDb.LastName = lastName;
                context.SaveChanges();
            }
        }

        public void ChangeEmail(string email)
        {
            using (var context = Mvx.IoCProvider.IoCConstruct<PostgreSqlContext>())
            {
                var userDb = context.Users.Single(u => u.UserId == _userService.CurrentUser.UserId);
                userDb.Email = email;
                context.SaveChanges();
            }
        }

        public void ChangeBirthdate(DateTime birthdate)
        {
            using (var context = Mvx.IoCProvider.IoCConstruct<PostgreSqlContext>())
            {
                var userDb = context.Users.Single(u => u.UserId == _userService.CurrentUser.UserId);
                userDb.Birthdate = birthdate;
                context.SaveChanges();
            }
        }

        public double GetDiscountFactorFromDatabase(int hours)
        {
            double factor = 1;
            IEnumerable<DiscountDb> discountList;
            using (var context = Mvx.IoCProvider.IoCConstruct<PostgreSqlContext>())
            {
                discountList = context.Discounts.ToList();
            }

            foreach (var discount in discountList)
            {
                if (hours >= discount.StartHour)
                {
                    factor = discount.Factor;
                }
                else
                {
                    break;
                }
            }
            return factor;
        }

        public double GetDiscountCodeFactorFromDatabase(string code)
        {
            double factor = 1;
            using (var context = Mvx.IoCProvider.IoCConstruct<PostgreSqlContext>())
            {
                if (!context.DiscountCodes.Any(x => x.Code == code))
                {
                    // TODO 
                    // Give message that code is not found.
                    Console.WriteLine("Code not found from database.");
                }
                else
                {
                    var discountCodeDb = context.DiscountCodes.Single(x => x.Code == code);
                    factor = discountCodeDb.Factor;
                }
            }
            return factor;
        }
    }
}
