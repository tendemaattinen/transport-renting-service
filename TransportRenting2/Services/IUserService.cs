﻿using TransportRenting.Core.Models;

namespace TransportRenting.Core.Services
{
    public interface IUserService
    {
        IUser CurrentUser { get; }
        bool IsUserLoogedIn { get; }

        void LogUserIn(IUser user);
        void LogUserOut();
    }
}