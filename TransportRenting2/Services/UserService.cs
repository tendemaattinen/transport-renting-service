﻿using System;
using System.Collections.Generic;
using System.Text;
using TransportRenting.Core.Models;

namespace TransportRenting.Core.Services
{
    public class UserService : IUserService
    {
        public IUser CurrentUser { get; private set; }
        public bool IsUserLoogedIn { get; private set; } = false;

        public void LogUserIn(IUser user)
        {
            CurrentUser = user;
            IsUserLoogedIn = true;
        }

        public void LogUserOut()
        {
            CurrentUser = null;
            IsUserLoogedIn = false;
        }
    }
}
