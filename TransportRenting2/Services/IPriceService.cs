﻿using System;
using System.Threading.Tasks;

namespace TransportRenting.Core.Services
{
    public interface IPriceService
    {
        double CalculatePrice(double basePrice, DateTime startingDate, DateTime endingDate, string discountCode = null);
    }
}