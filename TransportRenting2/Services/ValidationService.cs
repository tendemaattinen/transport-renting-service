﻿using MvvmCross;
using System;
using TransportRenting.Core.Models;
using TransportRenting.Core.Utilities;

namespace TransportRenting.Core.Services
{
    public class ValidationService : IValidationService
    {
        readonly IDatabaseService _databaseService;
        readonly IAgeValidation _ageValidation;
        public ValidationService(IDatabaseService databaseService, IAgeValidation ageValidation)
        {
            _databaseService = databaseService;
            _ageValidation = ageValidation;
        }

        public bool ValidateRegistration(User user, string password, string password2, Action<string> giveMessage)
        {
            // TODO
            // IoC??
            var usernameMinLength = 5;
            var usernameMaxLength = 50;
            var args = new { usernameMinLength, usernameMaxLength };

            var validator = Mvx.IoCProvider.IoCConstruct<UserValidation>(args);
            var results = validator.Validate(user);
            int minimumAge = 18;

            if (!results.IsValid)
            {
                // Gives first of all errors to user.
                giveMessage(results.Errors[0].ErrorMessage);
                return false;
            }
            else if (!_ageValidation.ValidateAge(user, DateTime.UtcNow, minimumAge))
            {
                giveMessage($"Minimum age is {minimumAge}!");
                return false;
            }
            else if (_databaseService.CheckIfUserExistsInDatabase(user.Username))
            {
                giveMessage("Username already exists!");
                return false;
            }
            else if (!(CheckPasswordValidity(password, password2, 5, 50, (message) => giveMessage(message))))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool CheckPasswordValidity(string password, string password2, int minLenght, int maxLenght, Action<string> giveMessage)
        {
            bool validatePassword = false;
            if (password != password2)
            {
                giveMessage("Passwords won't match!");
            }
            else if (password.Length == 0)
            {
                giveMessage("Passwords can't be empty!");
            }
            else if (password.Length < minLenght)
            {
                giveMessage($"Minimum length of password is {minLenght} characters!");
            }
            else if (password.Length > maxLenght)
            {
                giveMessage($"Maximum length of password is {maxLenght} characters!");
            }
            else
            {
                validatePassword = true;
            }
            return validatePassword;
        }
    }
}
