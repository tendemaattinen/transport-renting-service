﻿using System;

namespace TransportRenting.Core.Services
{
    public class PriceService : IPriceService
    {
        readonly ITimeService _timeService;
        readonly IDiscountService _discountService;
        public PriceService(ITimeService timeService, IDiscountService discountService)
        {
            _timeService = timeService;
            _discountService = discountService;
        }

        public double CalculatePrice(double basePrice, DateTime startingDate, DateTime endingDate, string discountCode = null)
        {
            // Users pays from starting hour, so time is always rounded up.
            int rentingTime = (int)Math.Ceiling(_timeService.CalculateNumberOfHours(startingDate, endingDate));
            double price =  _discountService.CalculateDiscountedPrice(basePrice, rentingTime, discountCode);
            return price * rentingTime;
        }
    } 
}
