﻿namespace TransportRenting.Core.Services
{
    public interface IDiscountService
    {
        double CalculateDiscountedPrice(double price, int hours, string discounCode);
    }
}