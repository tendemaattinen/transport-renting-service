﻿using System;
using System.Threading.Tasks;

namespace TransportRenting.Core.Services
{
    public interface ILoginService
    {
        bool Login(string username, string password, Action<string> giveMessage);
    }
}