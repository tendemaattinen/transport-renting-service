﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransportRenting.Core.Services
{
    public class TimeService : ITimeService
    {
        public double CalculateNumberOfHours(DateTime startDate, DateTime endDate)
        {
            return (endDate - startDate).TotalHours;
        }
    }
}
