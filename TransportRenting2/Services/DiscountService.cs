﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransportRenting.Core.Services
{
    public class DiscountService : IDiscountService
    {
        readonly IDatabaseService _databaseService;
        public DiscountService(IDatabaseService databaseService)
        {
            _databaseService = databaseService;
        }

        public double CalculateDiscountedPrice(double price, int hours, string discountCode)
        {
            double codeFactor = 1;

            if (discountCode != null)
            {
                codeFactor = _databaseService.GetDiscountCodeFactorFromDatabase(discountCode);
            }

            var factor = _databaseService.GetDiscountFactorFromDatabase(hours);

            return price * factor * codeFactor;
        }
    }
}
