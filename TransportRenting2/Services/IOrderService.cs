﻿using TransportRenting.Core.Models;

namespace TransportRenting.Core.Services
{
    public interface IOrderService
    {
        IOrder CurrentOrder { get; set; }
    }
}